(function($) {
  Drupal.behaviors.qasCommnets = {
    attach : function(context, settings) {
      jQuery('.node-qas-wrapper .comment-form', context).hide();      
      jQuery('.node-qas-wrapper .qasCommentsFormTitle', context).click(function(){
        $(this).hide().next('.node-qas-wrapper .comment-form').show('fast');
        return false;
      });
    }
  };
}(jQuery));
